# Tutorial 2 :  Generating Training Data in ArGIS

## Prerequisites :

1. Activated ArcGIS python environment as specified in `Tutorial 1 : ArcGIS Setup for Deep Learning`

2. All prerequisites specified in `Tutorial 1`

## Motivation for Labelling Images
A key component of training deep learning models is to provide ample labelled data.

In the case of training models to perform pixelwise classification of satellite imagery, we require labelled image masks.. i.e, we want to label pixels according to their thematic class, such as `tree`,`water`, `grass\bush`, `bare soil` and `impervious surface`. The deep learning model, in this case a convolutional neural network, will associate the unique characteristics of a given column of pixels with its respective class. After seeing many examples of pixels from each class, the network will learn low and highdimensional semantic relationships between isolated and constructive pixel spectral qualities the underyling true class. Building a high quality training dataset to allow the model to learn these semantics is one of the most important components of any deep learning project.

## ArcGIS Classification Toolset
ArcGIS provides a great protocal for generating image masks in the `Classification Toolset` under the Imagery Ribbon. Within the `Classification Toolset` is the `Label Objects for Deep Learning` annotation tool. This tool will allow the analyist to overlay polygons, rectangles, circles and freehand draw different class extents atop basemap imagery. See image one below for reference. 

![](docs/images/classification_tools.png)

After selecting a class from the `5-Class-Schema`, the analyst can also select a shape to draw with. After drawing a shape, the pixels encopassed in the shape will be labelled as the choosen class. This will appear in the `Labeled Objects` pane, along with the percentage of the labelled data present in one object.

## Export Training Data

Once ample examples have been labelled, we are ready to export  the training data to disk memory. Within the Image Classification Pane, there is a subpane titled `Export Training Data`. Navigating to the subpane, we are faced with some parameter choices. Please fill in the function parameters as indicated in Figure 2. The most import part of this is that the `Meta Data Format` is set to `Classified Tiles`, which will format the output data so that its ready for the specific deep learning task ahead, semantic segmentation.

![Image classificatin pane](docs/images/export_img.jpg)

Additional specified parameters such as Tile Size and Stride, indicate the dimension of the output example tiles and the amount over lamp between examples. The choice of 256 is a hyper-parameter of the model that may change depending on predictive performance.

Once the function parameters have been filled in as specified, click the `Run` button in the bottom right corner of the `Image Classification` Pane. Once complete, new directories for the Image and Label tiles will be exported to the specified `Output Folder`.
