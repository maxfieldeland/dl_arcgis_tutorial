# Tutorial Four : How to train a (dragon) neural network 

## Prerequisites

1. Must have user account with UVM VACC/DeepGreen
2. Must have ability to SSH or FTTP onto VACC server from current machine
3. Knowledge of git version control.

## Tutorial Description
This is one of the most complicated components of this project. The first section will focus on setting up the correct python environment on DeepGreen. Then using the `SAL_segmentation` repo, we will preprocess labelled data, define a Keras model and fit the model to the data. This process will produce a `.h5` file that can then be used according to Tutorial 3. Note: There is no guarantee that a neural network will be able to fit a dataset. Without proper training and experience with tuning neural networks, this chance reduces greatly. However, this tutorial is designed for someone with command line experience and minimal python to produce neural networks. That does not mean that they will always work. This tutorial is intended to abstract much of the process, allowing folks with many different backgrounds to interact with DeepGreen and deep learning. It will walk you through a series of command line code snippets, without requiring you to edit any code files.

### Step One : Establishing a connection with the VACC 

#### Resources
The VACC is a powerful cluster of CPU and GPU cores that are optimized to perform inference and model training. In addition to the fine VACC faculty, there are some great online resources to learn how it works, and how its resources are configured to be used by us! Please consult the [VACC Wiki](https://wiki.uvm.edu/w/DeepGreenDocs#What_is_the_difference_between_a_node.2C_a_task_and_a_GPU.3F_is_the_mem_referring_to_mem_per_node.3F_How_much_can_we_put.3F) for additional information

1. After opening a bash shell(unix/linux) or Putty Session (windows) connect to the VACC server using SSH or FTP protocol. Download putty if needed : https://www.putty.org/. 

    * if using bash 
        * `$ ssh uvmnetid@vacc-user1.uvm.edu` 
        * `$ cd scratch/`
        * `$ git clone https://gitlab.com/maxfieldeland/impervious_surface_segmentation.git`
        * `$ cd impervious_surface_segmentation`
    * if using putty : enter credentials through GUI. 

2. [*REQUIRED ONLY ONCE PER USER*] Now we need to setup your VACC user with Anaconda. We will use the `setup_conda.sh` bash script to download and install the most recent version of Anaconda.  Then we will activate the `conda` python environment necessary to use deep learning tools stored in the `impervious.yml` file. 

    * `$ sh setup_conda.sh`

Now you should notices that the terminal environment has changed from `base` to `impervious`.

* `(impervious) [mgreen13@vacc-user1 impervious_surface_segmentation]$ `

### Step Two : Preparing Example Data
In this step, we will be preparing data for semantic segmentation. For this example, we will be using thematic raster from Cambridge, MA and an accompanying 4 Band NAIP Image. The preprocessing steps will stack and tile the large rasters into small stacks of data that will train the model. Additionally, it will create the training file directory structure and throw out any unlabelled sections of the thematic raster. 

To start the preprocessing step : 

`$ python pipeline.py cambridge`

This will produce a tiles tensor that holds the original images sliced into tiles and stack atop one another saved into a `numpy` array. 

### Step Three : Train U-NET Neural Network
In this step, we will use the DeepGreen SLURM scheduler to enter a `job` into the job queue. The job request consists of a bash script that contains information about how much memory and GPU cores the computing task will require. 

First we will need to ssh from the VACC server to the DeepGreen server: 

`$ ssh dg-user1`

Now, we will need to navigate to the directory holding out DeepGreen bash script. 

`$ cd src/bash_scripts/`

To launch the job, we will use the `sbatch` command. See the DeepGreen wiki for alterative launch options.

`$ sbatch unetMSE_sbatch.sh`

Now, the job has been launched. To check on the jobs status in the queue, use the command `qstat`.
Once the status of the job goes from `running` to `complete`, the model weight set will be saved to the `models` subdirectory. 
