
# Tutorial 3 : Semantic Segmentation on Example Imagery 

## Prerequisites 
1. Sucessfully complete all of Tutorial 1. 

### Using a Pre-trained model in ArcGIS
Now that the python tools are installed and conda environment is configured, we are ready to use ArcGIS to generate a thematic map from example 4 band NIAP imagery. 

1. Create a new Project using a Map template
2. Add this folder `dl_arcgis_tutorial` to project using `Add Folder`
3. Within project catalog, right click on imagery and select `Add to Current Map`.
This image tile is a chip selected from a larger Mosaic raster of Rhode Island NIAP. 
4. Open the `Geoprocessing Toolbox`. Find and select `Classify Pixels Using Deep Learning`

5. Fill in parameters as follows 
        a. Input Raster : `example_RI_imagery.tif`
        b. Output Raster : `classified_example_RI_imagery.tif`
        c. Model Definition : `azure_pixel_level_land_classification.emd`
6. Execute protocol by clicking `Run`

The model will take 5-15 minutes to compute on the input data depending on the hardware specs of the machine used. You will be notified by ArcGIS at the 25\%, 50\% and 75\% process completion stages. 

Once completed, a new raster will be added to the Drawing Order pane that holds the models predictions. Toggle the predictions on and off to view.

## Extending Process

This tutorial shows demonstrates how to use a pretrianed model to classify the pixels of a small example image tile. We can imagine a case where we want to change the input image, or even the model being used. The first use case is simple,  point the `Input Raster` parameter towards any tile or mosiac file that contains imagery. The caveot is that it must be captured by the same instrument/satellite. In this case the model is expecting 4-Band NAIP imagery. If we feed in 8-Band Landsat imagery, the model will not compile. 


### Changing the model specified 
In order to change the model we must switch the specified model definition (`.emd`) file. Each `.emd` file is unique to a specific file. Please see `Tutorial 4 : Writing .emd Files` to customize an `.emd` file to integrate with an alternative model and framework. 