
# Deep Learning in ArcGIS Tutorial Repository
#### Universiy of Vermont SAL

This repository hosts all material for the Deep Learning in ArcPro tutorial. Please see `ArcGIS_Deep_Learning.pdf` for further instructions. 

Created by Maxfield Green with adaptations from ESRI raster function repository https://github.com/Esri/raster-deep-learning.

In the `tutorials\` subdirectory, please see markdown files to get started with Deep Learning in ArcGIS.

Table of Contents 
1. [Tutorial One : Getting Started](/tutorials/tutorial_1_getting_started.md)
2. [Tutorial Two : Image Labelling ](/tutorials/tutorial_2_image_labelling.md)
3. [Tutorial Three : Semantic Segmentation](/tutorials/tutorial_3_pixel_classification.md)

Coming up next:
4. Writing custom `.emd` files and `Raster Functions`
5. Training new models using SAL Deep Learning API

Please contact mgreen13@uvm.edu for any questions. 