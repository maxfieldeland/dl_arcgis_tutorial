import numpy as np
import arcpy
from arcpy.ia import *
arcpy.CheckOutExtension("ImageAnalyst")

 

# Set local variables
in_raster = "C:\\Users\\mgree\\Desktop\\dl_arcgis_tutorial\\src\\example_RI_imagery.tif"
in_model_definition = "C:\\Users\\mgree\\Desktop\\dl_arcgis_tutorial\\src\\custom_segmentation.emd"

# Check out the ArcGIS Image Analyst extension license

# Execute 
Out_classified_raster = ClassifyPixelsUsingDeepLearning(in_raster,in_model_definition)
Out_classified_raster.save("classified_RI_new.tif")




# model from file == true